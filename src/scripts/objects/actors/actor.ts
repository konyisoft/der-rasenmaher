import { game } from '../../game';
import { Axis, AxisDirection, Blocker, Position } from '../../common/types';
import { MainScene } from '../../scenes/main-scene';
import { Utils } from '../../common/utils';
import { CONFIG, TILE_PROPERTY } from '../../common/constants';

// A base class for all movable game sprites: player, items, enemies
export abstract class Actor extends Phaser.Physics.Arcade.Sprite {

  protected mainScene: MainScene;
  protected moveSpeed: number = 0.07;
  protected currentPixelPosition: Position;  // pixels
  protected desiredPixelPosition: Position;  // pixels
  protected currentAxis: Axis;
  protected currentAxisDirection: AxisDirection;
  protected isMoving: boolean;
  protected isPaused: boolean;

  constructor(scene: Phaser.Scene, gridX: number, gridY: number, spriteId: string) {
    super(scene, 0, 0, spriteId);  // call original ctor with zero position
    this.scene.add.existing(this);
    this.mainScene = scene as MainScene;
    this.isMoving = false;
    this.currentPixelPosition = this.desiredPixelPosition = this.gridToPixelPosition({ x: gridX, y: gridY });
    this.setPosition(this.currentPixelPosition.x, this.currentPixelPosition.y);
  }

  update(time: number, delta: number): void {
    // Paused?
    if (this.isPaused) {
      return;
    }

    // Is sprite currently moving?
    if (this.isMoving) {
      let axis: string = this.currentAxis;
      let direction: number = this.desiredPixelPosition[axis] < this.currentPixelPosition[axis] ? -1 : 1;
      this.currentPixelPosition[axis] += delta * this.moveSpeed * direction;
      // Reached the desired point?
      if (
        (direction < 0 && this.currentPixelPosition[axis] <= this.desiredPixelPosition[axis]) ||
        (direction > 0 && this.currentPixelPosition[axis] >= this.desiredPixelPosition[axis])
      ) {
        this.currentPixelPosition[axis] = this.desiredPixelPosition[axis];
        this.isMoving = false;
        this.moveEnded();
      }
      // Apply position
      this.setPosition(this.currentPixelPosition.x, this.currentPixelPosition.y);
    }

    this.checkCollisions();
  }

  move(axis: Axis, axisDirection: AxisDirection, options?: any): void {
    this.isMoving = true;
    this.currentAxis = axis;
    this.currentAxisDirection = axisDirection;
    this.desiredPixelPosition = Utils.clone(this.currentPixelPosition);
    this.desiredPixelPosition[axis] += axisDirection * CONFIG.TILE.SIZE;
  }

  canMoveTo(blockers: Blocker, ...destinations: Position[]): boolean {
    // Check all destinations / blocker type
    let origin: Position = this.Position;
    for (let i = 0; i < destinations.length; i++) {
      let position: Position = Utils.addPositions(origin, destinations[i]);
      if (
        // Item
        ((blockers & Blocker.Item) === Blocker.Item && game.itemManager.isItemAt(position)) ||
        // Enemy
        ((blockers & Blocker.Enemy) === Blocker.Enemy && game.enemyManager.isEnemyAt(position)) ||
        // Player
        ((blockers & Blocker.Player) === Blocker.Player && game.playerManager.Player!.equalsAnyPosition(position))
      ) {
        return false;
      }
      let tile: Phaser.Tilemaps.Tile = game.mapManager.getTileAt(position);
      if (tile == null) {
        continue;
      }
      if (
        // Block
        ((blockers & Blocker.Block) === Blocker.Block && tile.properties[TILE_PROPERTY.BLOCK]) ||
        // Water
        ((blockers & Blocker.Water) === Blocker.Water && tile.properties[TILE_PROPERTY.WATER]) ||
        // Flower
        ((blockers & Blocker.Flower) === Blocker.Flower && tile.properties[TILE_PROPERTY.FLOWER])
      ) {
        return false;
      }
    }
    return true;
  }

  pause(): void {
    this.isPaused = true;
    this.anims.pause();
  }

  resume(): void {
    this.isPaused = false;
    this.anims.resume();
  }

  get IsIdle(): boolean {
    return !this.isMoving;
  }

  get IsPaused(): boolean {
    return this.isPaused;
  }

  get Position(): Position {
    return this.pixelToGridPosition({ x: this.x, y: this.y });
  }

  get Bounds(): Phaser.Geom.Rectangle {
    return new Phaser.Geom.Rectangle(this.x - 4, this.y - 4, 8, 8); // smaller bounds
  }

  protected moveEnded(): void {
  }

  protected checkCollisions(): void {
  }

  protected gridToPixelPosition(position: Position): Position {
    return {
      x: (position.x * CONFIG.TILE.SIZE) + CONFIG.TILE.HALFSIZE,
      y: (position.y * CONFIG.TILE.SIZE) + CONFIG.TILE.HALFSIZE
    };
  }

  protected pixelToGridPosition(position: Position): Position {
    return {
      x: position.x / CONFIG.TILE.SIZE | 0,
      y: position.y / CONFIG.TILE.SIZE | 0
    };
  }
}
