
import { game } from '../../game';
import { Actor } from './actor';
import { Enemy } from './enemy';
import { TILE_PROPERTY } from '../../common/constants';

export class Item extends Actor {

  protected checkCollisions(): void {
    // Check item bounds against enemies bounds
    let bounds: Phaser.Geom.Rectangle = this.Bounds;
    for (let i = 0; i < game.enemyManager.Enemies.length; i++) {
      let enemy: Enemy = game.enemyManager.Enemies[i];
      if (Phaser.Geom.Intersects.RectangleToRectangle(bounds, enemy.Bounds)) {
        this.mainScene.enemyTouched(enemy);
        return;
      }
    }
  }

  protected moveEnded(): void {
    // Check tile below this item (not needed continuous check in checkCollisions)
    let tile: Phaser.Tilemaps.Tile = game.mapManager.getTileAt(this.Position);
    if (tile != null) {
      // Flowers
      if (tile.properties[TILE_PROPERTY.FLOWER]) {
        this.mainScene.destroyTile(tile);
        return;
      // Water
      } else if (tile.properties[TILE_PROPERTY.WATER]) {
        this.mainScene.fallIntoWater(this);
        return;
      }
    }
  }
}