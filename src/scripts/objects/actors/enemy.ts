import { Actor } from './actor';
import { Axis, AxisDirection, Blocker, EnemyState, Position } from '../../common/types';
import { Utils } from '../../common/utils';
import { ANIMATION } from '../../common/constants';

export interface EnemyConfig {
  canFly: boolean;
}

export class Enemy extends Actor {

  private counter: number;
  private currentState: EnemyState = EnemyState.None;
  private spriteId: string;
  private config: EnemyConfig;

  constructor(scene: Phaser.Scene, gridX: number, gridY: number, spriteId: string, config: EnemyConfig) {
    super(scene, gridX, gridY, spriteId);
    this.setDisplayOrigin(8, 8);
    this.moveSpeed = 0.025;
    this.spriteId = spriteId;
    this.config = config;
  }

  start(): void {
    // Select a random state
    if (this.currentState != EnemyState.Idle && !this.config.canFly && Utils.random(0, 1) == 0) {
      this.randomWait();
    } else {
      this.randomMove();
    }
  }

  update(time: number, delta: number): void {
    if (this.isPaused) {
      return;
    }

    // Idle state
    if (this.currentState == EnemyState.Idle) {
      this.counter -= delta/1000;
      if (this.counter <= 0) {
        this.waitEnded();
      }
    // Moving state
    } else if (this.currentState == EnemyState.Moving) {
      super.update(time, delta);
    }
  }

  get Config(): EnemyConfig {
    return this.config;
  }

  private randomWait() {
    this.currentState = EnemyState.Idle;
    this.counter = Utils.random(3, 7);  // seconds to wait
    this.anims.play(ANIMATION[this.spriteId].clip.idle.key, true);
  }

	private randomMove(): void {
    let attemptCount: number = 0;
    let canMoveTo: boolean = false;
    let axis: Axis = Axis.x;
    let axisDirection: AxisDirection = AxisDirection.Positive;
    let destination: Position = Utils.ZeroPosition;

    // Try ro find a random direction to move (max. 10 attempts)
    while (!canMoveTo && attemptCount < 10) {
      // Get random values
      axis = Utils.random(0, 1) == 0 ? Axis.x : Axis.y;
      axisDirection = Utils.random(0, 1) == 0 ? AxisDirection.Negative : AxisDirection.Positive;
      destination = Utils.ZeroPosition;
      destination[axis] = axisDirection;
      canMoveTo = this.canMoveTo(this.Blocker, destination);
      attemptCount++;
    }

    // Found a direction?
    if (canMoveTo) {
      this.currentState = EnemyState.Moving;
      this.counter = Utils.random(5, 15); // tiles to move in this direction
      this.move(axis, axisDirection);
      this.anims.play(this.getMoveAnimKey(axis, axisDirection), true);
    // Else wait for a while
    } else {
      this.randomWait();
    }
  }

  protected moveEnded(): void {
    this.counter--;
    if (this.counter > 0) {
      let nextDestination: Position = Utils.ZeroPosition;
      nextDestination[this.currentAxis] = this.currentAxisDirection;
      // Able to move any further?
      if (this.canMoveTo(this.Blocker, nextDestination)) {
        this.move(this.currentAxis, this.currentAxisDirection);
      // No, stop this movement
      } else {
        this.counter = 0;
      }
    }
    // Restart
    if (this.counter <= 0) {
      this.start();
    }
  }

  private waitEnded(): void {
    // Restart
    this.start();
  }

  private getMoveAnimKey(axis: Axis, axisDirection: AxisDirection): string {
    let key: string = '';

    // Simple move animation, if defined
    if (Utils.isDefined(ANIMATION[this.spriteId].clip.move)) {
      return ANIMATION[this.spriteId].clip.move.key;
    }

    // 4 direction move
    switch (axis) {
      case Axis.x:
        key = axisDirection == AxisDirection.Negative ?
          ANIMATION[this.spriteId].clip.left.key :
          ANIMATION[this.spriteId].clip.right.key;
        break;
      case Axis.y:
        key = axisDirection == AxisDirection.Negative ?
          ANIMATION[this.spriteId].clip.up.key :
          ANIMATION[this.spriteId].clip.down.key;
        break;
    }
    return key;
  }

  private get Blocker(): Blocker {
    let blocker: Blocker = Blocker.All;
    if (this.config.canFly) {
      blocker &= ~Blocker.Flower;  // remove flower
      blocker &= ~Blocker.Water;  // remove water
    }
    return blocker;
  }
}
