import { game } from '../../game';
import { Actor } from './actor';
import { Enemy } from './enemy'
import { Item } from './item'
import { Axis, AxisDirection, Blocker, LookDirection, Position, Rotation } from '../../common/types';
import { Utils } from '../../common/utils';
import { SPRITE, TILE_PROPERTY, CONFIG, ANIMATION } from '../../common/constants';

export class Player extends Actor {

  private rotationSpeed: number = 0.4;
  private startAngle: number;
  private currentAngle: number;  // aka. look direction
  private desiredAngle: number;
  private isRotating: boolean;

  constructor(scene: Phaser.Scene, gridX: number, gridY: number, angle: number) {
    super(scene, gridX, gridY, SPRITE.PLAYER);
    this.startAngle = angle;
    this.anims.play(ANIMATION[SPRITE.PLAYER].clip.idle.key, true);
    this.setDisplayOrigin(8, 24); // rotates around this point
    this.setCrop(0, 0, 16, 31);   // crop the last line due to some glitches (bug?)
    this.isRotating = false;
    this.currentAngle = this.startAngle;
    this.setAngle(this.startAngle);
  }

  moveUp(): void {
    if (this.IsIdle && !this.isPaused) {
      switch (this.currentAngle) {
        case LookDirection.Right:
          this.rotate(Rotation.AntiClockwise);
          break;
        case LookDirection.Left:
          this.rotate(Rotation.Clockwise);
          break;
        default:
          this.move(Axis.y, AxisDirection.Negative, { isForward: this.isLookDirection(LookDirection.Up) });
      }
    }
  }

  moveDown(): void {
    if (this.IsIdle && !this.isPaused) {
      switch (this.currentAngle) {
        case LookDirection.Right:
          this.rotate(Rotation.Clockwise);
          break;
        case LookDirection.Left:
          this.rotate(Rotation.AntiClockwise);
          break;
        default:
          this.move(Axis.y, AxisDirection.Positive, { isForward: this.isLookDirection(LookDirection.Down) });
      }
    }
  }

  moveLeft(): void {
    if (this.IsIdle && !this.isPaused) {
      switch (this.currentAngle) {
        case LookDirection.Up:
          this.rotate(Rotation.AntiClockwise);
          break;
        case LookDirection.Down:
          this.rotate(Rotation.Clockwise);
          break;
        default:
          this.move(Axis.x, AxisDirection.Negative, { isForward: this.isLookDirection(LookDirection.Left) });
      }
    }
  }

  moveRight(): void {
    if (this.IsIdle && !this.isPaused) {
      switch (this.currentAngle) {
        case LookDirection.Up:
          this.rotate(Rotation.Clockwise);
          break;
        case LookDirection.Down:
          this.rotate(Rotation.AntiClockwise);
          break;
        default:
          this.move(Axis.x, AxisDirection.Positive, { isForward: this.isLookDirection(LookDirection.Right) });
      }
    }
  }

  update(time: number, delta: number): void {
    // Player paused?
    if (this.isPaused) {
      return;
    }

    // Rotating
    if (this.isRotating) {
      let direction: number = this.desiredAngle < this.currentAngle ? -1 : 1;
      this.currentAngle += delta * this.rotationSpeed * direction;
      // Reached the desired angle?
      if (
        (direction < 0 && this.currentAngle <= this.desiredAngle) ||
        (direction > 0 && this.currentAngle >= this.desiredAngle)
      ) {
        this.currentAngle = Utils.clampAngle360(this.desiredAngle);
        this.isRotating = false;
      }
      // Apply rotation
      this.setAngle(this.currentAngle);
    }

    // Call base method here (moving)
    super.update(time, delta);
  }

  move(axis: Axis, axisDirection: AxisDirection, options?: any): void {
    // Setup destination
    let playerDestination: Position = Utils.ZeroPosition;
    playerDestination[axis] = axisDirection * (options.isForward ? 2 : 1);
    // Moving item, if player is standing next to it
    let item: Item | null = game.itemManager.getItemAt(Utils.addPositions(this.Position, playerDestination));
    let itemMoved: boolean = false;
    let itemDestination: Position = Utils.ZeroPosition;
    itemDestination[axis] = axisDirection;
    if (item != null && item.canMoveTo(Blocker.Block | Blocker.Item, itemDestination)) {
      item.move(axis, axisDirection);
      itemMoved = true;
    }
    // Move the player, if possible
    if (itemMoved || this.canMoveTo(Blocker.Block | Blocker.Item, playerDestination)) {
      this.scene.time.removeAllEvents();
      this.anims.play(
        options.isForward ? ANIMATION[SPRITE.PLAYER].clip.forward.key : ANIMATION[SPRITE.PLAYER].clip.back.key,
        true
      );
      super.move(axis, axisDirection); // call base method
    }
  }

  pause(): void {
    this.anims.play(ANIMATION[SPRITE.PLAYER].clip.idle.key, true);
    super.pause();
  }

  equalsAnyPosition(position: Position): boolean {
    return Utils.arePositionsEqual(position, this.Position) || Utils.arePositionsEqual(position, this.LawnmowerPosition);
  }

  get IsIdle(): boolean {
    return !this.isRotating && !this.isMoving;
  }

  get LawnmowerBounds(): Phaser.Geom.Rectangle {
    let position: Position = this.LawnmowerPixelPosition;
    return new Phaser.Geom.Rectangle(position.x - 4, position.y - 4, 8, 8); // smaller bounds
  }

  protected moveEnded(): void {
    // Play idle animation with 100 millisecs delay
    this.scene.time.addEvent({
      delay: 100,
      callback: () => {
        if (!this.isMoving) {
          this.anims.play(ANIMATION[SPRITE.PLAYER].clip.idle.key, true);
        }
      }
    });
  }

  protected checkCollisions(): void {
    // Check tile below the player
    let tileOfPlayer: Phaser.Tilemaps.Tile = game.mapManager.getTileAt(this.Position);
    if (tileOfPlayer != null) {
      // Water
      if (tileOfPlayer.properties[TILE_PROPERTY.WATER]) {
        this.mainScene.waterTouched();
        return;
      }
    }
    // Check tile below the lawnmower
    let tileOfLawnmower: Phaser.Tilemaps.Tile = game.mapManager.getTileAt(this.LawnmowerPosition);
    if (tileOfLawnmower != null) {
      // Grass and flowers
      if (tileOfLawnmower.properties[TILE_PROPERTY.FLOWER] || tileOfLawnmower.properties[TILE_PROPERTY.GRASS]) {
        this.mainScene.destroyTile(tileOfLawnmower);
      // Water
      } else if (tileOfLawnmower.properties[TILE_PROPERTY.WATER]) {
        this.mainScene.waterTouched();
        return;
      }
    }
    // Check player bounds against enemies bounds
    let bounds: Phaser.Geom.Rectangle = this.Bounds;
    let lawnmowerBounds: Phaser.Geom.Rectangle = this.LawnmowerBounds;
    for (let i = 0; i < game.enemyManager.Enemies.length; i++) {
      let enemy: Enemy = game.enemyManager.Enemies[i];
      if (
        Phaser.Geom.Intersects.RectangleToRectangle(bounds, enemy.Bounds) ||
        Phaser.Geom.Intersects.RectangleToRectangle(lawnmowerBounds, enemy.Bounds)
      ) {
        this.mainScene.enemyTouched(enemy);
        return;
      }
    }
  }

  private get LawnmowerPixelPosition(): Position {
    return {
      x: CONFIG.TILE.SIZE * Math.cos(Utils.toRad(this.currentAngle - 90)) + this.x,
      y: CONFIG.TILE.SIZE * Math.sin(Utils.toRad(this.currentAngle - 90)) + this.y
    }
  }

  private get LawnmowerPosition(): Position {
    return this.pixelToGridPosition(this.LawnmowerPixelPosition);
  }

  private rotate(rotation: Rotation): void {
    let destinations: Position[] = [];
    let toggle: number = rotation == Rotation.Clockwise ? 1 : -1;
    switch (this.currentAngle) {
      case LookDirection.Up:
        destinations = [{ x: 1 * toggle, y: -1 }, { x: 1 * toggle, y: 0 }];
      break;
      case LookDirection.Down:
        destinations = [{ x: -1 * toggle, y: 1 }, { x: -1 * toggle, y: 0 }];
        break;
      case LookDirection.Left:
        destinations = [{ x: -1, y: -1 * toggle }, { x: 0, y: -1 * toggle }];
        break;
      case LookDirection.Right:
        destinations = [{ x: 1, y: 1 * toggle }, { x: 0, y: 1 * toggle }];
        break;
    }
    // Rotating the player, if possible
    if (this.canMoveTo(Blocker.Block | Blocker.Item, ...destinations)) {
      this.isRotating = true;
      this.desiredAngle = this.currentAngle + rotation;
    }
  }

  private isLookDirection(lookDirection: LookDirection): boolean {
    return lookDirection == this.currentAngle;
  }
}
