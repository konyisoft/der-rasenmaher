import { CONFIG, DEPTH } from '../../common/constants';

export class GameOverText extends Phaser.GameObjects.Text {

  constructor(scene: Phaser.Scene) {
    super(scene, 0, 0, '', { color: 'red', fontSize: '2rem', fontFamily: 'mikropix' });
    scene.add.existing(this);
    this.setText('GAME OVER');
    this.setAlign('center');
    this.setOrigin(0.5, 0.5);
    this.setPosition(CONFIG.CANVAS.WIDTH / 2, CONFIG.CANVAS.HEIGHT / 2);
    this.setShadow(2, 2, 'rgba(0,0,0,0.75');
    this.setDepth(DEPTH.UI);
  }

  reset() {
    this.hide();
  }

  hide() {
    this.setVisible(false);
  }

  show() {
    this.setVisible(true);
  }

}
