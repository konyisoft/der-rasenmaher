import { CONFIG, DEPTH } from '../../common/constants';

export class LevelDoneText extends Phaser.GameObjects.Text {

  constructor(scene: Phaser.Scene) {
    super(scene, 0, 0, '', { color: 'white', fontSize: '2rem', fontFamily: 'mikropix' });
    scene.add.existing(this);
    this.setText('LEVEL DONE!');
    this.setAlign('center');
    this.setOrigin(0.5, 0.5);
    this.setPosition(CONFIG.CANVAS.WIDTH / 2, CONFIG.CANVAS.HEIGHT / 2);
    this.setShadow(2, 2, 'rgba(0,0,0,0.75');
    this.setDepth(DEPTH.UI);
  }

  reset() {
    this.hide();
  }

  hide() {
    this.setVisible(false);
  }

  show() {
    this.setVisible(true);
  }

}
