import { Globals } from '../../common/globals';
import { CONFIG, DEPTH } from '../../common/constants';

export class ScoreText extends Phaser.GameObjects.Text {

  constructor(scene: Phaser.Scene) {
    super(scene, 0, 0, '', { color: 'yellow', fontSize: '2rem', fontFamily: 'mikropix' });
    scene.add.existing(this);
    this.setOrigin(0);
    this.setAlign('right');
    this.setShadow(2, 2, 'rgba(0,0,0,0.75');
    this.setDepth(DEPTH.UI);
  }

  reset(): void {
    this.displayScore(0);
  }

  refresh(): void {
    this.displayScore(Globals.score);
  }

  private displayScore(score: number) {
    this.setText(score + '');
    this.setColor(score < 0 ? 'red' : 'yellow');
    this.setPosition(CONFIG.CANVAS.WIDTH - this.width - 2, -6);
  }
}
