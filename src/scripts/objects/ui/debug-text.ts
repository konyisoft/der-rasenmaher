import { DEPTH } from '../../common/constants';

export class DebugText extends Phaser.GameObjects.Text {

  constructor(scene: Phaser.Scene) {
    super(scene, 4, 2, '', { color: 'white', fontSize: '.5rem', fontFamily: 'mikropix' });
    scene.add.existing(this);
    this.setOrigin(0);
    this.setDepth(DEPTH.UI);
  }
}
