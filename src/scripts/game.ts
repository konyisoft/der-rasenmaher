import 'phaser'
import { EnemyManager, ItemManager, InputManager, MapManager, PlayerManager, UiManager } from './managers';
import { MainScene } from './scenes/main-scene'
import { PreloadScene } from './scenes/preload-scene'
import { CONFIG } from './common/constants'

const config: Phaser.Types.Core.GameConfig = {
  type: Phaser.AUTO,
  backgroundColor: 'rgb(18,18,18)',
  input: {
    keyboard: true,
  },
  scale: {
    parent: 'phaser-game',
    mode: Phaser.Scale.FIT,
    autoCenter: Phaser.Scale.CENTER_BOTH,
    width: CONFIG.CANVAS.WIDTH,
    height: CONFIG.CANVAS.HEIGHT,
  },
  render: {
    pixelArt: true,
  },
  scene: [PreloadScene, MainScene],
  physics: {
    default: 'arcade',
    arcade: {
      debug: false,
    }
  },
}

export class Game extends Phaser.Game {

  enemyManager: EnemyManager;
  itemManager: ItemManager;
  inputManager: InputManager;
  mapManager: MapManager;
  playerManager: PlayerManager;
  uiManager: UiManager;

  constructor(config?: Phaser.Types.Core.GameConfig) {
    super(config);
    this.createMangers();
  }

  private createMangers(): void {
    // Create managers
    this.enemyManager = new EnemyManager();
    this.inputManager = new InputManager();
    this.itemManager = new ItemManager();
    this.mapManager = new MapManager();
    this.playerManager = new PlayerManager();
    this.uiManager = new UiManager();
  }
}

export let game: Game;

window.addEventListener('load', () => {
  game = new Game(config);
})
