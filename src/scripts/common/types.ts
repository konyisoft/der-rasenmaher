export enum GameState {
  Playing,
  GameOver,
  Restarting,
}

export interface Position {
  x: number;
  y: number;
}

export enum AxisDirection {
  Negative = -1,
  Positive = 1,
}

export enum LookDirection {
  Up = 0,
  Right = 90,
  Down = 180,
  Left = 270,
}

export enum Rotation {
  AntiClockwise = -90,
  Clockwise = 90
}

export enum Axis {
  x = 'x',
  y = 'y'
}

export enum EnemyState {
  None,
  Idle,
  Moving
}

export enum Blocker {
  None = 0,
  Block = 1 << 0,
  Water = 1 << 1,
  Flower = 1 << 2,
  Item = 1 << 3,
  Enemy = 1 << 4,
  Player = 1 << 5,
  All = ~(~0 << 6)
}

export interface Dictionary<T> {
  [id: string]: T;
}
