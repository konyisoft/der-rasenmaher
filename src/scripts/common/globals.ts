export class Globals {

	public static levelIndex: number = 0;
	public static score: number = 0;
	public static lives: number = 0;

	public static reset(): void {
		Globals.levelIndex = 0;
		Globals.score = 0;
		Globals.lives = 0;
	}

	public static increaseScore(value: number): void {
		// Value can be negative
		Globals.score += value;
	}

	public static loseLife(): void {
		if (Globals.lives > 0) {
			Globals.lives--;
		}
	}

}