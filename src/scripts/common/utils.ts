import { Position  } from './types';

export class Utils {

  static clone(obj: any): any {
    return JSON.parse(JSON.stringify(obj));
  }

  static random(min: number, max: number): number {
    min = Math.ceil(min);
    max = Math.floor(max);
    return Math.floor(Math.random() * (max - min + 1)) + min;
  }

  static get ZeroPosition(): Position {
    return { x: 0, y: 0 };
  }

  static arePositionsEqual(p1: Position, p2: Position): boolean {
    return p1.x === p2.x && p1.y === p2.y;
  }

  static addPositions(...positions: Position[]): Position {
    let p: Position = Utils.ZeroPosition;
    for (let i = 0; i < positions.length; i++) {
      p.x += positions[i].x;
      p.y += positions[i].y;
    }
    return p;
  }

  static clampAngle360(angle: number): number {
    // Clamps angle between 0 and 359
    return (angle + Math.ceil(-angle / 360) * 360) | 0;
  }

  static toRad(angle: number): number {
    return angle * Math.PI / 180;
  }

  static isDefined(obj: any): boolean {
    return typeof obj !== 'undefined';
  }

}