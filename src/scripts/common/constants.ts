export const CONFIG = {
  GRID: {
    WIDTH: 30,
    HEIGHT: 16
  },
  TILE: {
    SIZE: 16,  // both width and height
    HALFSIZE: 8
  },
  get CANVAS() {
    return {
      WIDTH: this.GRID.WIDTH * this.TILE.SIZE,
      HEIGHT: this.GRID.HEIGHT * this.TILE.SIZE
    }
  },
}

// Assets directory
export const DIRECTORY = {
  FONTS: 'assets/fonts/',
  MAPS: 'assets/maps/',
  JS: 'assets/js/',
  IMAGES: {
    SPRITES: 'assets/images/sprites/',
    TILESETS: 'assets/images/tilesets/'
  }
}

export const GAMEPLAY = {
  SCORE: {
    GRASS: 10,
    FLOWER: -100
  },
}

export const SCENE = {
  PRELOAD: 'preload-scene',
  MAIN: 'main-scene',
}

export const LAYER = {
  UNDERLYING: 0,
  LAYOUT: 1,
  SPAWN: 2,
}

export const TILESET = {
  COMMON: {
    IMAGE: 'common',
    TILESET: 'common',
  },
}

export const TILE_PROPERTY = {
  ANGLE: 'angle',
  BLOCK: 'block',
  CAN_FLY: 'canFly',
  ENEMY: 'enemy',
  FLOWER: 'flower',
  GRASS: 'grass',
  ITEM: 'item',
  PLAYER: 'player',
  SPRITE: 'sprite',
  WATER: 'water',
}

export const DEPTH = {
  UI: 5000,
  PLAYER: 1000,
  ITEM: 900,
  ENEMY: 800,
}

export const SPRITE = {
  BALL: 'ball',
  BLOOD_SPLASH: 'blood-splash',
  BUTTERFLY_BLUE: 'butterfly-blue',
  BUTTERFLY_PINK: 'butterfly-pink',
  BUTTERFLY_YELLOW: 'butterfly-yellow',
  CAT_BLACK: 'cat-black',
  CAT_WHITE: 'cat-white',
  CHAIR: 'chair',
  PARASOL: 'parasol',
  PLAYER: 'player',
  ROCK: 'rock',
  WATER_SPLASH: 'water-splash',
}

export const ANIMATION = {
  'player': {
    frameWidth: 16,
    frameHeight: 32,
    endFrame: 18,
    clip: {
      idle: { key: 'player-idle', start: 0, end: 0, repeat: 0 },
      forward: { key: 'player-forward', start: 1, end: 18, repeat: -1 },
      back: { key: 'player-back', start: 18, end: 1, repeat: -1 },
    }
  },
  'cat-black': {
    frameWidth: 16,
    frameHeight: 16,
    endFrame: 8,
    clip: {
      idle: { key: 'cat-black-idle', start: 0, end: 0, repeat: 0 },
      up: { key: 'cat-black-up', start: 1, end: 4, repeat: -1 },
      down: { key: 'cat-black-down', start: 5, end: 8, repeat: -1 },
      left: { key: 'cat-black-left', start: 1, end: 4, repeat: -1 },
      right: { key: 'cat-black-right', start: 5, end: 8, repeat: -1 },
    }
  },
  'cat-white': {
    frameWidth: 16,
    frameHeight: 16,
    endFrame: 8,
    clip: {
      idle: { key: 'cat-white-idle', start: 0, end: 0, repeat: 0 },
      up: { key: 'cat-white-up', start: 1, end: 4, repeat: -1 },
      down: { key: 'cat-white-down', start: 5, end: 8, repeat: -1 },
      left: { key: 'cat-white-left', start: 1, end: 4, repeat: -1 },
      right: { key: 'cat-white-right', start: 5, end: 8, repeat: -1 },
    }
  },
  'butterfly-blue': {
    frameWidth: 16,
    frameHeight: 16,
    endFrame: 3,
    clip: {
      idle: { key: 'butterfly-blue-idle', start: 0, end: 3, repeat: -1 },
      move: { key: 'butterfly-blue-move', start: 0, end: 3, repeat: -1 },
    }
  },
  'butterfly-pink': {
    frameWidth: 16,
    frameHeight: 16,
    endFrame: 3,
    clip: {
      idle: { key: 'butterfly-pink-idle', start: 0, end: 3, repeat: -1 },
      move: { key: 'butterfly-pink-move', start: 0, end: 3, repeat: -1 },
    }
  },
  'butterfly-yellow': {
    frameWidth: 16,
    frameHeight: 16,
    endFrame: 3,
    clip: {
      idle: { key: 'butterfly-yellow-idle', start: 0, end: 3, repeat: -1 },
      move: { key: 'butterfly-yellow-move', start: 0, end: 3, repeat: -1 },
    }
  },
  'blood-splash': {
    frameWidth: 16,
    frameHeight: 16,
    endFrame: 2,
    clip: {
      splash: { key: 'blood-splash', start: 0, end: 2, repeat: 0 },
    }
  },
  'water-splash': {
    frameWidth: 16,
    frameHeight: 16,
    endFrame: 5,
    clip: {
      splash: { key: 'water-splash', start: 0, end: 5, repeat: 0 },
    }
  },
}

export const SPRITE_FRAME_RATE: number = 16;

