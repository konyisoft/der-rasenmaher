import { Item } from '../objects/actors/item';
import { Position } from '../common/types';
import { DEPTH } from '../common/constants';

export class ItemManager {

  private items: Item[] = [];

  update(time: number, delta: number): void {
    this.items.forEach((item: Item) => {
      item.update(time, delta);
    });
  }

  createItem(scene: Phaser.Scene, gridX: number, gridY: number, spriteId: string) {
    let item: Item = new Item(scene, gridX, gridY, spriteId);
    this.items.push(item);
    item.setDepth(DEPTH.ITEM);
}

  getItemAt(position: Position): Item | null {
    // Iterate through items
    for (let i = 0; i < this.items.length; i++) {
      let itemPostion: Position = this.items[i].Position;
      if (position.x == itemPostion.x && position.y == itemPostion.y) {
        return this.items[i];
      }
    }
    return null;
  }

  isItemAt(position: Position): boolean {
    return this.getItemAt(position) != null;
  }

  pauseAll(): void {
    this.items.forEach((item: Item) => {
      item.pause();
    });
  }

  destroyItem(item: Item): void {
    let index: number = this.items.indexOf(item);
    if (index > -1) {
      this.items.splice(index, 1);
    }
    item.destroy();
  }

  destroyAll(): void {
    this.items.forEach((item: Item) => {
      item.destroy();
    });
    this.items = [];
  }

  get Items(): Item[] {
    return this.items;
  }
}