import { Player } from '../objects/actors';
import { DEPTH } from '../common/constants';

export class PlayerManager {

  private player: Player | null;

  createPlayer(scene: Phaser.Scene, gridX: number, gridY: number, angle: number) {
    this.player = new Player(scene, gridX, gridY, angle);
    this.player.setDepth(DEPTH.PLAYER); // bring to top
  }

  destroyPlayer(): void {
    this.player!.destroy();
    this.player = null;
  }

  pauserPlayer(): void {
    this.player!.pause();
  }

  get HasPlayer(): boolean {
    return this.player != null;
  }

  get Player(): Player | null {
    return this.player;
  }
}