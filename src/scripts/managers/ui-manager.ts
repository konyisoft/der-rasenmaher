import { DebugText, GameOverText, LevelDoneText, ScoreText } from '../objects/ui';

export class UiManager {

  private scoreText: ScoreText;
  private gameOverText: GameOverText;
  private levelDoneText: LevelDoneText;
  private debugText: DebugText;

  initUI(scene: Phaser.Scene): void {
    this.scoreText = new ScoreText(scene);
    this.gameOverText = new GameOverText(scene);
    this.levelDoneText = new LevelDoneText(scene);
    this.debugText = new DebugText(scene);
    this.scoreText.reset();
    this.gameOverText.reset();
    this.levelDoneText.reset();
  }

  setDebugText(text: string): void {
    this.debugText.setText(text);
  }

  showGameOverText(): void {
    this.gameOverText.show();
  }

  hideGameOverText(): void {
    this.gameOverText.hide();
  }

  showLevelDoneText(): void {
    this.levelDoneText.show();
  }

  hideLevelDoneText(): void {
    this.levelDoneText.hide();
  }

  refreshScoreText(): void {
    this.scoreText.refresh();
  }

  destroyUI(): void {
    this.scoreText.destroy();
    this.gameOverText.destroy();
    this.levelDoneText.destroy();
    this.debugText.destroy();
  }
}