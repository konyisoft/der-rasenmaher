export class InputManager {

  private keys: any = {};

  initKeys(scene: Phaser.Scene) {
    // Create keyboard control: direction keys and alternatives
    this.keys = scene.input.keyboard.addKeys({
      'up': Phaser.Input.Keyboard.KeyCodes.UP,
      'upAlt': Phaser.Input.Keyboard.KeyCodes.W,
      'down': Phaser.Input.Keyboard.KeyCodes.DOWN,
      'downAlt': Phaser.Input.Keyboard.KeyCodes.S,
      'left': Phaser.Input.Keyboard.KeyCodes.LEFT,
      'leftAlt': Phaser.Input.Keyboard.KeyCodes.A,
      'right': Phaser.Input.Keyboard.KeyCodes.RIGHT,
      'rightAlt': Phaser.Input.Keyboard.KeyCodes.D,
      'continue': Phaser.Input.Keyboard.KeyCodes.SPACE
    });
  }

  get Keys(): any {
    return this.keys
  }
}