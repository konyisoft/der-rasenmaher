import { Position } from '../common/types';
import { LAYER, TILESET } from '../common/constants';

export class MapManager {

  private currentMap: Phaser.Tilemaps.Tilemap;

  initMap(scene: Phaser.Scene): void {
    // Map
    this.currentMap = scene.make.tilemap({ key: 'map-1' });
    // Tiles
    this.currentMap.addTilesetImage(TILESET.COMMON.TILESET, TILESET.COMMON.IMAGE);
    // Layers
    this.currentMap.createStaticLayer(LAYER.UNDERLYING, TILESET.COMMON.TILESET);
    this.currentMap.createDynamicLayer(LAYER.LAYOUT, TILESET.COMMON.TILESET);
    this.currentMap.createStaticLayer(LAYER.SPAWN, "");  // no tileset added
  }

  getTileAt(position: Position): Phaser.Tilemaps.Tile {
    return this.currentMap.getTileAt(position.x, position.y, false, LAYER.LAYOUT);
  }

  get CurrentMap(): Phaser.Tilemaps.Tilemap {
    return this.currentMap;
  }
}