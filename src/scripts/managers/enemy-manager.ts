import { Enemy, EnemyConfig } from '../objects/actors';
import { Position } from '../common/types';
import { DEPTH } from '../common/constants';

export class EnemyManager {

  private enemies: Enemy[] = [];

  update(time: number, delta: number): void {
    this.enemies.forEach((enemy: Enemy) => {
      enemy.update(time, delta);
    });
  }

  createEnemy(scene: Phaser.Scene, gridX: number, gridY: number, spriteId: string, config: EnemyConfig) {
    let enemy: Enemy = new Enemy(scene, gridX, gridY, spriteId, config);
    this.enemies.push(enemy);
    enemy.setDepth(DEPTH.ENEMY);
  }

  getEnemyAt(position: Position): Enemy | null {
    // Iterate through enemies
    for (let i = 0; i < this.enemies.length; i++) {
      let enemyPostion: Position = this.enemies[i].Position;
      if (position.x == enemyPostion.x && position.y == enemyPostion.y) {
        return this.enemies[i];
      }
    }
    return null;
  }

  isEnemyAt(position: Position): boolean {
    return this.getEnemyAt(position) != null;
  }

  moveAll(): void {
    // Start to move ememies
    for (let i = 0; i < this.enemies.length; i++) {
      this.enemies[i].start();
    }
  }

  pauseAll(): void {
    this.enemies.forEach((enemy: Enemy) => {
      enemy.pause();
    });
  }

  destroyEnemy(enemy: Enemy): void {
    let index: number = this.enemies.indexOf(enemy);
    if (index > -1) {
      this.enemies.splice(index, 1);
    }
    enemy.destroy();
  }

  destroyAll(): void {
    this.enemies.forEach((enemy: Enemy) => {
      enemy.destroy();
    });
    this.enemies = [];
  }

  get Enemies(): Enemy[] {
    return this.enemies;
  }
}