import { game } from '../game';
import { Enemy, Item, Player } from '../objects/actors';
import { GameState } from '../common/types';
import { Globals } from '../common/globals';
import { CONFIG, GAMEPLAY, LAYER, SCENE, SPRITE, TILE_PROPERTY, ANIMATION } from '../common/constants';

export class MainScene extends Phaser.Scene {

  private state: GameState;
  private remainingTileCount: number;

  constructor() {
    super({ key: SCENE.MAIN });
  }

  create(): void  {
    // Order is important here (z-index!)
    game.inputManager.initKeys(this);
    game.mapManager.initMap(this);
    game.uiManager.initUI(this);
    this.createActors();
    this.remainingTileCount = this.getRemainingTileCount();
    this.cameras.main.fadeFrom(1000, 18, 18, 18);
    this.setState(GameState.Playing);
  }

  update(time: number, delta: number): void {
    if (this.state == GameState.Restarting) {
      return;
    }

    let keys: any = game.inputManager.Keys;

    if (this.state == GameState.GameOver && keys.continue.isDown) {
      this.restart();
      return;
    }

    let player: Player | null= game.playerManager.Player; // shortcut
    if (player != null) {
      if (player.IsIdle) {
        // Up
        if (keys.up.isDown || keys.upAlt.isDown) {
          player.moveUp();
        }
        // Down
        else if (keys.down.isDown || keys.downAlt.isDown) {
          player.moveDown();
        }
        // Left
        else if (keys.left.isDown || keys.leftAlt.isDown) {
          player.moveLeft();
        }
        // Right
        else if (keys.right.isDown || keys.rightAlt.isDown) {
          player.moveRight();
        }
      }

      // Update player, items, enemies
      player.update(time, delta);
      game.itemManager.update(time, delta);
      game.enemyManager.update(time, delta);

      // TODO: remove this later
      game.uiManager.setDebugText(
        `FPS: ${this.game.loop.actualFps | 0}\n` +
        `PX: ${player.x | 0} - PY: ${player.y | 0}\n` +
        `GX: ${player.Position.x} - GY: ${player.Position.y}\n` +
        `REMAIN: ${this.remainingTileCount}`
      );
    }
  }

  destroyTile(tile: Phaser.Tilemaps.Tile): void {
    // Decrease remaining tile count
    if (tile != null) {
      // Grass: increasing score
      if (tile.properties[TILE_PROPERTY.GRASS]) {
        this.remainingTileCount--;
        Globals.increaseScore(GAMEPLAY.SCORE.GRASS);
        // Level done?
        if (this.remainingTileCount == 0) {
          this.levelDone();
        }
        // Flower: decreasing (negative score added)
      } else if (tile.properties[TILE_PROPERTY.FLOWER]) {
        Globals.increaseScore(GAMEPLAY.SCORE.FLOWER);
      }
      game.mapManager.CurrentMap.removeTile(tile);
      game.uiManager.refreshScoreText();
    }
  }

  waterTouched(): void {
    // TODO: some animation
    this.gameOver();
  }

  enemyTouched(enemy: Enemy): void {
    // Destroy enemy
    game.enemyManager.destroyEnemy(enemy);
    // Add some blood
    this.add.sprite(enemy.x, enemy.y, SPRITE.BLOOD_SPLASH).anims.play(ANIMATION[SPRITE.BLOOD_SPLASH].clip.splash.key);
    // Game over
    this.gameOver();
  }

  fallIntoWater(item: Item): void {
    // Destroy item
    game.itemManager.destroyItem(item);
    // Add some blood
    this.add.sprite(item.x, item.y, SPRITE.WATER_SPLASH).anims.play(ANIMATION[SPRITE.WATER_SPLASH].clip.splash.key);
  }

  gameOver(): void {
    game.playerManager.pauserPlayer();
    game.enemyManager.pauseAll();
    game.itemManager.pauseAll();
    game.uiManager.showGameOverText();
    this.setState(GameState.GameOver);
  }

  restart(): void {
    this.setState(GameState.Restarting);

    this.cameras.main.on('camerafadeoutcomplete', () => {
      game.playerManager.destroyPlayer();
      game.enemyManager.destroyAll();
      game.itemManager.destroyAll();
      game.uiManager.destroyUI();
      Globals.reset();
      this.cameras.main.off('camerafadeoutcomplete');
      this.scene.restart();
    });

    this.cameras.main.fade(1000, 0, 0, 0);
  }

  levelDone(): void {
    game.playerManager.pauserPlayer();
    game.enemyManager.pauseAll();
    game.itemManager.pauseAll();
    game.uiManager.showLevelDoneText();
    this.setState(GameState.GameOver);
  }

  private getRemainingTileCount(): number {
    // Count destroyable tiles on the level
    let count: number = 0;
    game.mapManager.CurrentMap.forEachTile((tile: Phaser.Tilemaps.Tile) => {
      if (tile != null && tile.properties[TILE_PROPERTY.GRASS]) {
        count++;
      }
    }, this, 0, 0, CONFIG.GRID.WIDTH, CONFIG.GRID.HEIGHT, {}, LAYER.LAYOUT);
    return count;
  }

  private createActors(): void {
    // Iterate through spawn layer tiles and create sprites on spawn points
    game.mapManager.CurrentMap.forEachTile((tile: Phaser.Tilemaps.Tile) => {
      // Items
      if (tile.properties[TILE_PROPERTY.ITEM]) {
        game.itemManager.createItem(this, tile.x, tile.y, tile.properties[TILE_PROPERTY.SPRITE]);
      // Enemies
      } else if (tile.properties[TILE_PROPERTY.ENEMY]) {
        game.enemyManager.createEnemy(this, tile.x, tile.y, tile.properties[TILE_PROPERTY.SPRITE], {
          canFly: tile.properties[TILE_PROPERTY.CAN_FLY],
        });
      // Player (only on the first spawn point found)
      } else if (tile.properties[TILE_PROPERTY.PLAYER]) {
        game.playerManager.createPlayer(this, tile.x, tile.y, tile.properties[TILE_PROPERTY.ANGLE]);
      }
    }, this, 0, 0, CONFIG.GRID.WIDTH, CONFIG.GRID.HEIGHT, {}, LAYER.SPAWN);

    // Start all enemies to move
    game.enemyManager.moveAll();
  }

  private setState(state: GameState): void {
    this.state = state;
  }
}
