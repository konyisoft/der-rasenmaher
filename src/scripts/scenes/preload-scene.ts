import { ANIMATION, DIRECTORY, SCENE, SPRITE, SPRITE_FRAME_RATE, TILESET } from '../common/constants';

declare var WebFont: any;

export class PreloadScene extends Phaser.Scene {

  private fontLoaded: boolean = false;

  constructor() {
    super({ key: SCENE.PRELOAD });
  }

  preload(): void {
    this.load.on('progress', this.onLoadProgress, this);
    this.load.on('complete', this.onLoadComplete, this);
    this.loadSprites();
    this.loadTilemaps();
    this.loadScripts();
  }

  create(): void {
    this.createAnimations();
    this.createFonts();
  }

  update(): void {
    if (this.fontLoaded) {
      this.scene.start(SCENE.MAIN);
    }
  }

  private onLoadProgress(progress: number): void {
    // Not used at this time
    // console.log(`${Math.round(progress * 100)}%`);
  }

  private onLoadComplete(loader: any, completed: number, failed: number): void {
    console.log('Loading finished');
    console.log('Completed: ', completed);
    console.log('Failed: ', failed);
  }

  private loadScripts(): void {
    //this.load.script('webfont', 'https://ajax.googleapis.com/ajax/libs/webfont/1.6.26/webfont.js'); // load from googleapis
    this.load.script('webfont', DIRECTORY.JS + 'webfont/webfont.js');
  }

  private loadSprites(): void {
    Object.keys(SPRITE).forEach((key) => {
      let spriteId: string = SPRITE[key];
      let anim: any = ANIMATION[spriteId];
      // Animated? Load as spritesheet.
      if (anim) {
        this.load.spritesheet(spriteId, DIRECTORY.IMAGES.SPRITES + spriteId + '.png', {
          frameWidth: anim.frameWidth,
          frameHeight: anim.frameHeight,
          endFrame: anim.endFrame,
        });
      // Not animated. Load as image.
      } else {
        this.load.image(spriteId, DIRECTORY.IMAGES.SPRITES + spriteId + '.png');
      }
    });
  }

  private loadTilemaps(): void {
    this.load.image(TILESET.COMMON.IMAGE, DIRECTORY.IMAGES.TILESETS + 'common.png');
    this.load.tilemapTiledJSON('map-1', DIRECTORY.MAPS + 'map-1.json');
  }

  private createAnimations(): void {
    Object.keys(SPRITE).forEach((key) => {
      let spriteId: string = SPRITE[key];
      let anim: any = ANIMATION[spriteId];
      if (anim) {
        // Create animations
        Object.keys(anim.clip).forEach((clipKey) => {
          let clip: any = anim.clip[clipKey];
          this.anims.create({
            key: clip.key,
            frames: this.anims.generateFrameNumbers(spriteId, {
              start: clip.start,
              end: clip.end,
            }),
            frameRate: SPRITE_FRAME_RATE,
            repeat: clip.repeat,
          });
        });
      }
    });
  }

  private createFonts(): void {
    WebFont.load({
      custom: {
        families: ['mikropix']
      },
      active: () => {
        this.fontLoaded = true;  // wait for the font to be loaed
      }
    });
  }
}
