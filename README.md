# Der Rasenmäher

![Screenshot](src/assets/misc/og.png)


## About

Der Rasenmäher is a prototype of a pixel art logical game written in [Phaser 3](https://phaser.io/) and TypeScript. The purpose of the game is to mow the lawn in the garden. You can move or rotate the lawnmower with the **ARROW** or **W-A-S-D** keys. Try not to kill the little animals and avoid touching water and flowers.

## Demo

A playable version of the game [can be found here.](https://konyisoft.gitlab.io/der-rasenmaher/)

## Install

To try out this project, first you will have [Node.js](https://nodejs.org/en/) and [Git](https://git-scm.com/) installed on your computer.

Download (clone) the files of the project to your local computer:

```
$ git clone git@gitlab.com:konyisoft/der-rasenmaher.git
```

Navigate to the project directory and run the following command to install the required packages:

```
$ cd der-rasenmaher/
$ npm install
```

Open the project in your default browser with this command:

```
$ npm start
```

To create the production build into the */dist* directory, type the following:

```
$ npm run build
```

## License

MIT

----

Krisztian Konya, Konyisoft, 2020  
[konyisoft.eu](https://konyisoft.eu/)
